package org.apache.jsp._03;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class applesAndOranges_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>\r\n");
      out.write("  <head>\r\n");
      out.write("    <title>Comparing Apples and Oranges</title>\r\n");
      out.write("  </head>\r\n");
      out.write("\r\n");
      out.write("  <body>\r\n");
      out.write("  <center>\r\n");
      out.write("    <h2>Comparing Apples and Oranges</h2>\r\n");
      out.write("\r\n");
      out.write("    ");
String format = request.getParameter("format");
        if ((format != null) && (format.equals("excel"))) {
          response.setContentType("application/vnd.ms-excel");
        } else if ((format != null) && (format.equals("pdf"))) {
          response.setContentType("application/pdf");
        }
      
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <table border=\"1\">\r\n");
      out.write("      <tr><th/><th>Apples</th><th>Oranges</th></tr>\r\n");
      out.write("      <tr><th>First Quarter</th><td>2307</td><td>4706</td></tr>\r\n");
      out.write("      <tr><th>Second Quarter</th><td>2982</td><td>5104</td></tr>\r\n");
      out.write("      <tr><th>Third Quarter</th><td>3011</td><td>5220</td></tr>\r\n");
      out.write("      <tr><th>Fourth Quarter</th><td>3055</td><td>5287</td></tr>\r\n");
      out.write("    </table>\r\n");
      out.write("\r\n");
      out.write("  </center>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
