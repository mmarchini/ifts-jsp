package org.apache.jsp._03;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;

public final class checkbox_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("  <head>\r\n");
      out.write("    <title>Fruits Selection</title>\r\n");
      out.write("  </head>\r\n");
      out.write("\r\n");
      out.write("  <body bgcolor=\"white\">\r\n");
      out.write("\r\n");
      out.write("    <form action=\"checkbox.jsp\">\r\n");
      out.write("      <input type=\"checkbox\" name=\"fruits\" value=\"Apple\">Apple<br/>\r\n");
      out.write("      <input type=\"checkbox\" name=\"fruits\" value=\"Banana\">Banana<br/>\r\n");
      out.write("      <input type=\"checkbox\" name=\"fruits\" value=\"Orange\">Orange<br/>\r\n");
      out.write("      <input type=\"submit\" value=\"Invia\">\r\n");
      out.write("    </form>\r\n");
      out.write("\r\n");
      out.write("  ");
String[] picked = request.getParameterValues("fruits");
    if (picked != null && picked.length != 0) { 
      Vector vPicked=new Vector();
      for (int i=0;i<picked.length;i++) vPicked.add(picked[i]); 
      out.write("\r\n");
      out.write("      You picked the following fruits:\r\n");
      out.write("      <form>\r\n");
      out.write("        <input type=\"checkbox\" name=\"fruits\" value=\"Apple\"\r\n");
      out.write("          ");
      out.print( vPicked.contains("Apple") ? "checked=\"checked\"" : "" );
      out.write("/>Apple<br/>\r\n");
      out.write("        <input type=\"checkbox\" name=\"fruits\" value=\"Banana\"\r\n");
      out.write("          ");
      out.print( vPicked.contains("Banana") ? "checked=\"checked\"" : "" );
      out.write("/>Banana<br/>\r\n");
      out.write("        <input type=\"checkbox\" name=\"fruits\" value=\"Orange\"\r\n");
      out.write("          ");
      out.print( vPicked.contains("Orange") ? "checked=\"checked\"" : "" );
      out.write("/>Orange<br/>\r\n");
      out.write("      </form>\r\n");
      out.write("  ");
}
      out.write("\r\n");
      out.write("\r\n");
      out.write("  </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
