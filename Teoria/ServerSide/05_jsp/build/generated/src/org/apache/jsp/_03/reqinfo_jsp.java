package org.apache.jsp._03;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class reqinfo_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("  <body bgcolor=\"white\">\r\n");
      out.write("    The following information was received:\r\n");
      out.write("    <ul>\r\n");
      out.write("      <li>Request Method: ");
      out.print( request.getMethod());
      out.write("</li>\r\n");
      out.write("      <li>Request URI: ");
      out.print( request.getRequestURI());
      out.write("</li>\r\n");
      out.write("      <li>Request Protocol: ");
      out.print( request.getProtocol());
      out.write("</li>\r\n");
      out.write("      <li>Servlet Path: ");
      out.print( request.getServletPath());
      out.write("</li>\r\n");
      out.write("      <li>Query String: ");
      out.print( request.getQueryString());
      out.write("</li>\r\n");
      out.write("      <li>Server Name: ");
      out.print( request.getServerName());
      out.write("</li>\r\n");
      out.write("      <li>Server Port: ");
      out.print( request.getServerPort());
      out.write("</li>\r\n");
      out.write("      <li>Remote Address: ");
      out.print( request.getRemoteAddr());
      out.write("</li>\r\n");
      out.write("      <li>Remote Host: ");
      out.print( request.getRemoteHost());
      out.write("</li>\r\n");
      out.write("      <li>Browser Type: ");
      out.print( request.getHeader("User-Agent"));
      out.write("</li>\r\n");
      out.write("    </ul>\r\n");
      out.write("  </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
