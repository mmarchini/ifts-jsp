package org.apache.jsp._06;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class WhatsNew_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>\r\n");
      out.write("  <head>\r\n");
      out.write("    <title>What's New</title>\r\n");
      out.write("  </head>\r\n");
      out.write("\r\n");
      out.write("  <body>\r\n");
      out.write("\r\n");
      out.write("    <table border=\"5\" align=\"center\">\r\n");
      out.write("      <tr><th>What's New at JspNews.com</th></tr>\r\n");
      out.write("    </table>\r\n");
      out.write("\r\n");
      out.write("    <p>\r\n");
      out.write("      Here is a summary of our four most recent news stories:\r\n");
      out.write("      <ol>\r\n");
      out.write("        <li>");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "item1.txt", out, true);
      out.write("</li>\r\n");
      out.write("        <li>");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "item2.txt", out, true);
      out.write("</li>\r\n");
      out.write("        <li>");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "item3.txt", out, true);
      out.write("</li>\r\n");
      out.write("        <li>");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "item4.txt", out, true);
      out.write("</li>\r\n");
      out.write("      </ol>\r\n");
      out.write("    </p>\r\n");
      out.write("\r\n");
      out.write("  </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
