package org.apache.jsp._07;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class computeSpeed_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"speedErrors.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("  <head>\r\n");
      out.write("    <title>Computing Speed</title>\r\n");
      out.write("  </head>\r\n");
      out.write("\r\n");
      out.write("  <body>\r\n");
      out.write("\r\n");
      out.write("    <table border=\"5\" align=\"center\">\r\n");
      out.write("      <tr><th>Computing Speed</th></tr>\r\n");
      out.write("    </table>\r\n");
      out.write("\r\n");
      out.write("  ");
double distance = Double.valueOf(request.getParameter("distance")).doubleValue(); 
    double time = Double.valueOf(request.getParameter("time")).doubleValue();
    double speed = distance/time;
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <ul>\r\n");
      out.write("      <li>Distance: ");
      out.print( distance );
      out.write(" m</li>\r\n");
      out.write("      <li>Time: ");
      out.print( time );
      out.write(" s</li>\r\n");
      out.write("      <li>Speed: ");
      out.print( speed );
      out.write(" m/s</li>\r\n");
      out.write("    </ul>\r\n");
      out.write("\r\n");
      out.write("  </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
