package org.apache.jsp._10;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.Enumeration;

public final class esempioSession_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("\r\n");
 String azione = request.getParameter("azione");

  if (azione != null && (azione.equals("1") || azione.equals("2"))) {
    session.setAttribute(request.getParameter("nome"), request.getParameter("valore"));
  }

  if (azione != null && azione.equals("3")) {
    session.removeAttribute(request.getParameter("nome"));
  }

  if (azione != null && azione.equals("4")) {
    session.invalidate();
  }


      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("  <head>\r\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("    <title>Esempio Sessione</title>\r\n");
      out.write("  </head>\r\n");
      out.write("  <body>\r\n");
      out.write("    <h1>Esempio Sessione</h1>\r\n");
      out.write("\r\n");
      out.write("    <form action=\"esempioSession.jsp\">\r\n");
      out.write("\r\n");
      out.write("      Nome   <input type=\"text\" name=\"nome\" value=\"\"/><br/>\r\n");
      out.write("      Valore <input type=\"text\" name=\"valore\" value=\"\"/><br/>\r\n");
      out.write("\r\n");
      out.write("      <select name=\"azione\">\r\n");
      out.write("        <option value=\"1\">Inserisci</option>\r\n");
      out.write("        <option value=\"2\">Modifica</option>\r\n");
      out.write("        <option value=\"3\">Cancella</option>\r\n");
      out.write("        <option value=\"4\">Invalida sessione</option>\r\n");
      out.write("      </select><br/>\r\n");
      out.write("\r\n");
      out.write("      <input type=\"submit\" value=\"Ok\"/><br/>\r\n");
      out.write("\r\n");
      out.write("    </form>\r\n");
      out.write("\r\n");
      out.write("    Sessione: <br/>\r\n");
      out.write("\r\n");
      out.write("    ");
try {
      out.write("\r\n");
      out.write("\r\n");
      out.write("    Id: ");
      out.print( session.getId());
      out.write(" <br/>    \r\n");
      out.write("    IsNew: ");
      out.print( session.isNew());
      out.write(" <br/>        \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    ");

      if (session.isNew()) {
        session.setMaxInactiveInterval(500);
      }

      Enumeration sessionNames = session.getAttributeNames();
      while (sessionNames.hasMoreElements()) {
        String sessionName = (String) sessionNames.nextElement();
        String sessionValue = (String) session.getAttribute(sessionName);
      out.write("\r\n");
      out.write("\r\n");
      out.write("    ");
      out.print(sessionName);
      out.write(" = ");
      out.print(sessionValue);
      out.write("<br/>\r\n");
      out.write("\r\n");
      out.write("    ");
 }

    } catch (IllegalStateException e) {
      out.write("\r\n");
      out.write("    Sessione non valida<br/>\r\n");
      out.write("\r\n");
      out.write("    ");
}
      out.write("\r\n");
      out.write("  </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
