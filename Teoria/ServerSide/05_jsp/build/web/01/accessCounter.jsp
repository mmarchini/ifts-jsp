<html>
  <head>
    <title>JSP Declarations</title>
  </head>

  <body>
    <h1>JSP Declarations</h1>
    
    <%! 
    
      public int sum(int a, int b) {
       return a+b; 
      }
    
    %>
    
    <%int accessCount = 0; %>
    <h2>Accesses to page since server reboot: <%= ++accessCount %></h2>
  </body>

</html>