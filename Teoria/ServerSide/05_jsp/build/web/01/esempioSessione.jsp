<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>

<%-- session.setMaxInactiveInterval(30);--%>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
  </head>
  <body>
    <h1>Esempio Sessione</h1>
    
    <p><%= session.isNew()%></p>
    <p><%= session.getId()%></p>    
    
  </body>
</html>
