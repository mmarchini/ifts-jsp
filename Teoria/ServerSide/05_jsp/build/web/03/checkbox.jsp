<%@ page language="java" contentType="text/html" %>
<%@ page import="java.util.*"%>
<html>
  <head>
    <title>Fruits Selection</title>
  </head>

  <body bgcolor="white">

    <form action="checkbox.jsp">
      <input type="checkbox" name="fruits" value="Apple">Apple<br/>
      <input type="checkbox" name="fruits" value="Banana">Banana<br/>
      <input type="checkbox" name="fruits" value="Orange">Orange<br/>
      <input type="submit" value="Invia">
    </form>

  <%String[] picked = request.getParameterValues("fruits");
    if (picked != null && picked.length != 0) { 
      Vector vPicked=new Vector();
      for (int i=0;i<picked.length;i++) vPicked.add(picked[i]); %>
      You picked the following fruits:
      <form>
        <input type="checkbox" name="fruits" value="Apple"
          <%= vPicked.contains("Apple") ? "checked=\"checked\"" : "" %>/>Apple<br/>
        <input type="checkbox" name="fruits" value="Banana"
          <%= vPicked.contains("Banana") ? "checked=\"checked\"" : "" %>/>Banana<br/>
        <input type="checkbox" name="fruits" value="Orange"
          <%= vPicked.contains("Orange") ? "checked=\"checked\"" : "" %>/>Orange<br/>
      </form>
  <%}%>

  </body>
</html>