<html>
  <head>
    <title>What's New</title>
  </head>

  <body>

    <table border="5" align="center">
      <tr><th>What's New at JspNews.com</th></tr>
    </table>

    <p>
      Here is a summary of our four most recent news stories:
      <ol>
        <li><jsp:include page="item1.txt" flush="true" /></li>
        <li><jsp:include page="item2.txt" flush="true" /></li>
        <li><jsp:include page="item3.txt" flush="true" /></li>
        <li><jsp:include page="item4.txt" flush="true" /></li>
      </ol>
    </p>

  </body>
</html>