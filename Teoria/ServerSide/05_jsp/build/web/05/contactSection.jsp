<%@ page import="java.util.Date" %>

<%! private int accessCount = 0;
    private Date accessDate = new Date();
    private String accessHost = "<I>No previous access</I>";%>

<p>
  <hr>
  This page &copy; 2003 
  <A HREF="http//www.my-company.com/">my-company.com</A>.
  This page has been accessed <%= ++accessCount %>
  times since server reboot. It was last accessed from 
  <%= accessHost %> at <%= accessDate %>.
  <% accessHost = request.getRemoteHost(); %>
  <% accessDate = new Date(); %>
</p>

