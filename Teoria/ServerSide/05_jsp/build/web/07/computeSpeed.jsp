<%@ page errorPage="speedErrors.jsp" %>
<html>
  <head>
    <title>Computing Speed</title>
  </head>

  <body>

    <table border="5" align="center">
      <tr><th>Computing Speed</th></tr>
    </table>

    <%
      try {
        double distance = Double.valueOf(request.getParameter("distance")).doubleValue();
        double time = Double.valueOf(request.getParameter("time")).doubleValue();
        double speed = distance / time;
    %>
    <ul>
      <li>Distance: <%= distance%> m</li>
      <li>Time: <%= time%> s</li>
      <li>Speed: <%= speed%> m/s</li>
    </ul>

    <%} catch (NullPointerException e) {%>
    Non mi hai forniti i valori necessari al calcolo    
    <%} catch (NumberFormatException e) {%>
    Mi devi fornire valori numerici  
    <%}%>
    
  </body>
</html>