<html>
  <head>
    <title>Comparing Apples and Oranges</title>
  </head>

  <body>
  <center>
    <h2>Comparing Apples and Oranges</h2>

    <%String format = request.getParameter("format");
        if ((format != null) && (format.equals("excel"))) {
          response.setContentType("application/vnd.ms-excel");
        } else if ((format != null) && (format.equals("word"))) {
          response.setContentType("application/msword");
        }
      %>

    <table border="1">
      <tr><th/><th>Apples</th><th>Oranges</th></tr>
      <tr><th>First Quarter</th><td>2307</td><td>4706</td></tr>
      <tr><th>Second Quarter</th><td>2982</td><td>5104</td></tr>
      <tr><th>Third Quarter</th><td>3011</td><td>5220</td></tr>
      <tr><th>Fourth Quarter</th><td>3055</td><td>5287</td></tr>
    </table>

  </center>
</body>
</html>