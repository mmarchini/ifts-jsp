<%@ page language="java" contentType="text/html" %>
<html>
  <head>
    <title>Fruits Selection</title>
  </head>

  <body bgcolor="white">

    <form action="loop.jsp">
      <input type="checkbox" name="fruits" value="Apple">Apple<br>
      <input type="checkbox" name="fruits" value="Banana">Banana<br>
      <input type="checkbox" name="fruits" value="Orange">Orange<br>
      <input type="submit" value="Enter">
    </form>

  <%String[] picked = request.getParameterValues("fruits");
    if (picked != null && picked.length != 0) {%>
      You picked the following fruits:
      <ul>
      <%for (int i = 0; i < picked.length; i++) {%>
          <li><%=picked[i]%></li>
      <%}%>
      </ul>
  <%}%>

  </body>
</html>