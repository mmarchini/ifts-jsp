<%@ page language="java" contentType="text/html" %>
<html>
  <body bgcolor="white">
    The following information was received:
    <ul>
      <li>Request Method: <%= request.getMethod()%></li>
      <li>Request URI: <%= request.getRequestURI()%></li>
      <li>Request Protocol: <%= request.getProtocol()%></li>
      <li>Servlet Path: <%= request.getServletPath()%></li>
      <li>Query String: <%= request.getQueryString()%></li>
      <li>Server Name: <%= request.getServerName()%></li>
      <li>Server Port: <%= request.getServerPort()%></li>
      <li>Remote Address: <%= request.getRemoteAddr()%></li>
      <li>Remote Host: <%= request.getRemoteHost()%></li>
      <li>Browser Type: <%= request.getHeader("User-Agent")%></li>
    </ul>
  </body>
</html>