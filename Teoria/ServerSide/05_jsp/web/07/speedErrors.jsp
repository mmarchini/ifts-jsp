<%@ page isErrorPage="true" %>
<html>
  <head>
    <title>Error Computing Speed</title>
  </head>

  <body>

    <table border="5" align="center">
      <tr><th>Error Computing Speed</th></tr>
    </table>
   
    <p>
      computeSpeed.jsp reported the following error: <i><%= exception %></i>.<br/>
      This problem occurred in the following place:
      <pre>
    <%exception.printStackTrace(new java.io.PrintWriter(out)); %>
      </pre>
    </p>
  </body>
</html>