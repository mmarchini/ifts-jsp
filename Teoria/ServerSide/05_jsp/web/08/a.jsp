<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%int fieldNumber=10;%>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Pagina A</title>
  </head>
  <body>
    <h1>Pagina A</h1>
    
    <form name="formA" action="b.jsp" method="post">
      
      <table>
        <tr><th>Item</th><th>Valore</th></tr>
        
        <% for (int i=0;i<fieldNumber;i++) { %>
        
        <tr>
          <td><input type="text" name="item<%=i%>" value=""/></td>
          <td><input type="text" name="valore<%=i%>" value=""/></td>
        </tr>
        
        <%}%>
                
        
      </table>
      
      <input type="hidden" name="fieldNumber" value="<%=fieldNumber%>"/>
      
      <input type="submit" value="Invia"/>
      
    </form>
    
  </body>
</html>
