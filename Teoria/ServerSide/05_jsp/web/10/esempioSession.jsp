<%@page import="java.util.Enumeration"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<% String azione = request.getParameter("azione");

  if (azione != null && (azione.equals("1") || azione.equals("2"))) {
    session.setAttribute(request.getParameter("nome"), request.getParameter("valore"));
  }

  if (azione != null && azione.equals("3")) {
    session.removeAttribute(request.getParameter("nome"));
  }

  if (azione != null && azione.equals("4")) {
    session.invalidate();
  }

%>


<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Esempio Sessione</title>
  </head>
  <body>
    <h1>Esempio Sessione</h1>

    <form action="esempioSession.jsp">

      Nome   <input type="text" name="nome" value=""/><br/>
      Valore <input type="text" name="valore" value=""/><br/>

      <select name="azione">
        <option value="1">Inserisci</option>
        <option value="2">Modifica</option>
        <option value="3">Cancella</option>
        <option value="4">Invalida sessione</option>
      </select><br/>

      <input type="submit" value="Ok"/><br/>

    </form>

    Sessione: <br/>

    <%try {%>

    Id: <%= session.getId()%> <br/>    
    IsNew: <%= session.isNew()%> <br/>        


    <%
      if (session.isNew()) {
        session.setMaxInactiveInterval(500);
      }

      Enumeration sessionNames = session.getAttributeNames();
      while (sessionNames.hasMoreElements()) {
        String sessionName = (String) sessionNames.nextElement();
        String sessionValue = (String) session.getAttribute(sessionName);%>

    <%=sessionName%> = <%=sessionValue%><br/>

    <% }

    } catch (IllegalStateException e) {%>
    Sessione non valida<br/>

    <%}
    
    Integer i=Integer.parseInt("12");
    String s=i.toString();
    int j=i.intValue();    
    Float.parseFloat(s);    
    Boolean.parseBoolean(s);    
    Double.parseDouble(s);
    
    
    %>
  </body>
</html>
