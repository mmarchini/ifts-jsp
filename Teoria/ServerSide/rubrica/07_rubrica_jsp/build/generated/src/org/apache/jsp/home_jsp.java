package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import blogics.*;

public final class home_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  public String getServletInfo() {
    return "Home Page";
  }

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(3);
    _jspx_dependants.add("/include/header.inc");
    _jspx_dependants.add("/include/aside.inc");
    _jspx_dependants.add("/include/footer.inc");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"/ErrorPage.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
User sUser = (User) session.getAttribute("sUser");
  boolean loggedOn = (sUser != null);

  int i;
  String action;
  Connection connection = null;

  Class.forName("com.mysql.jdbc.Driver");
  connection = DriverManager.getConnection(
          "jdbc:mysql://localhost/rubrica?user=rubrica&password=rubrica");
  connection.setAutoCommit(false);

  String userCode = request.getParameter("userCode");
  String password = request.getParameter("password");

  action = request.getParameter("action");
  if (action == null) {
    action = "view";
  }

  if (action.equals("logon")) {
    sUser = UserService.getUser(connection, userCode);
    if (sUser != null && sUser.password.equals(password)) {
      session.setAttribute("sUser", sUser);
      loggedOn = true;
    }
  }

  if (action.equals("logout")) {
    if (loggedOn) {
      session.removeAttribute("sUser");
      sUser=null;      
      loggedOn = false;
    }
  }

  connection.commit();
  connection.close();


      out.write("\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("  <head>\r\n");
      out.write("    <title>Rubrica - Logon</title>\r\n");
      out.write("    <meta name=\"author\" content=\"Mario Zambrini\"/>\r\n");
      out.write("    <link href=\"addressbook.css\" rel=\"stylesheet\" type=\"text/css\"/>\r\n");
      out.write("\r\n");
      out.write("    <script language=\"javascript\">\r\n");
      out.write("\r\n");
      out.write("      function isEmpty(value) {\r\n");
      out.write("\r\n");
      out.write("        if (value == null || value.length == 0)\r\n");
      out.write("          return true;\r\n");
      out.write("        for (var count = 0; count < value.length; count++) {\r\n");
      out.write("          if (value.charAt(count) != \" \")\r\n");
      out.write("            return false;\r\n");
      out.write("        }\r\n");
      out.write("        return true;\r\n");
      out.write("\r\n");
      out.write("      }\r\n");
      out.write("\r\n");
      out.write("      function submitLogon() {\r\n");
      out.write("\r\n");
      out.write("        if (isEmpty(document.logonForm.userCode.value)) {\r\n");
      out.write("          alert(\"Inserire uno username.\");\r\n");
      out.write("          return;\r\n");
      out.write("        }\r\n");
      out.write("\r\n");
      out.write("        if (isEmpty(document.logonForm.password.value)) {\r\n");
      out.write("          alert(\"Inserire una password.\");\r\n");
      out.write("          return;\r\n");
      out.write("        }\r\n");
      out.write("\r\n");
      out.write("        document.logonForm.submit();\r\n");
      out.write("      }\r\n");
      out.write("\r\n");
      out.write("    </script>\r\n");
      out.write("  </head>\r\n");
      out.write("\r\n");
      out.write("  <body>\r\n");
      out.write("\r\n");
      out.write("    <div id=\"container\">\r\n");
      out.write("\r\n");
      out.write("      ");
      out.write("<div id=\"header\">\r\n");
      out.write("  <h1>Rubrica</h1>\r\n");
      out.write("</div>\r\n");
      out.write("<div id=\"navigation\">\r\n");
      out.write("  <ul>\r\n");
      out.write("    <li><a href=\"home.jsp\">Home</a></li>\r\n");
      out.write("    ");
if (loggedOn) {
      out.write("\r\n");
      out.write("      <li><a href=\"AddressBookView.jsp\">Adressbook</a></li>\r\n");
      out.write("    ");
}
      out.write("\r\n");
      out.write("  </ul>\r\n");
      out.write("</div>");
      out.write(" \r\n");
      out.write("\r\n");
      out.write("      <div id=\"content-container\">\r\n");
      out.write("        <div id=\"content\">\r\n");
      out.write("          \r\n");
      out.write("          ");
if (!loggedOn) {
      out.write("\r\n");
      out.write("          <h2>Logon</h2>\r\n");
      out.write("          <br/><br/>\r\n");
      out.write("\r\n");
      out.write("          <form name=\"logonForm\" action=\"home.jsp\" method=\"post\">\r\n");
      out.write("\r\n");
      out.write("            <table border=\"0\">\r\n");
      out.write("              <tr>\r\n");
      out.write("                <td class=\"normal\" width=\"100\">Username</td>\r\n");
      out.write("                <td width=\"250\">\r\n");
      out.write("                  <input type=\"text\" name=\"userCode\" size=\"20\" maxlength=\"50\"/>\r\n");
      out.write("                </td>\r\n");
      out.write("              </tr>\r\n");
      out.write("              <tr>\r\n");
      out.write("                <td class=\"normal\" width=\"100\">Password</td>\r\n");
      out.write("                <td width=\"250\">\r\n");
      out.write("                  <input type=\"password\" name=\"password\" size=\"20\" maxlength=\"50\"/>\r\n");
      out.write("                </td>\r\n");
      out.write("              </tr>\r\n");
      out.write("            </table>\r\n");
      out.write("            <input type=\"hidden\" name=\"action\" value=\"logon\"/>     \r\n");
      out.write("            <input type=\"button\" value=\"Ok\" onClick=\"submitLogon();\"/>\r\n");
      out.write("          </form>\r\n");
      out.write("          ");
}
      out.write("\r\n");
      out.write("\r\n");
      out.write("          ");
if (loggedOn) {
      out.write("\r\n");
      out.write("          <h2>Home</h2>\r\n");
      out.write("          <br/><br/>\r\n");
      out.write("          Benvenuto ");
      out.print(sUser.firstname);
      out.write(' ');
      out.print(sUser.surname);
      out.write("!<br/>\r\n");
      out.write("          <form name=\"logoutForm\" action=\"home.jsp\" method=\"post\">\r\n");
      out.write("            <input type=\"hidden\" name=\"action\" value=\"logout\"/>\r\n");
      out.write("          </form>\r\n");
      out.write("          <a href=\"javascript:logoutForm.submit()\">Logout</a>\r\n");
      out.write("          ");
}
      out.write("\t\r\n");
      out.write("          \r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("        ");
      out.write("<div id=\"aside\">\r\n");
      out.write("  <h3>Rubrica</h3>\r\n");
      out.write("  ");
if (loggedOn) {
      out.write("\r\n");
      out.write("  <p>Benvenuto ");
      out.print(sUser.firstname);
      out.write(' ');
      out.print(sUser.surname);
      out.write("!<br/></p>\r\n");
      out.write("    ");
}
      out.write("\r\n");
      out.write("</div>");
      out.write(" \r\n");
      out.write("                \r\n");
      out.write("      </div>\r\n");
      out.write("        ");
      out.write("<div id=\"footer\">\r\n");
      out.write("  Corso IFTS, 2013-2014\r\n");
      out.write("</div>");
      out.write(" \r\n");
      out.write("    </div>        \r\n");
      out.write("\r\n");
      out.write("  </body>\r\n");
      out.write("\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
