
<%@ page info="Inserisci/Modifica Contatto" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="/ErrorPage.jsp" %>

<%@page import="blogics.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>

<%int i;

  String action;
  Connection connection = null;
  Contact contact = null;

  User sUser = (User) session.getAttribute("sUser");
  boolean loggedOn = (sUser != null);

  Class.forName("com.mysql.jdbc.Driver");
  connection = DriverManager.getConnection(
          "jdbc:mysql://localhost/rubrica?user=rubrica&password=rubrica");
  connection.setAutoCommit(false);

  action = request.getParameter("action");
  if (action == null) {
    action = "view";
  }

  String selectedInitial = request.getParameter("selectedInitial");
  Long contactCode = null;
  if (request.getParameter("contactCode") != null) {
    contactCode = Long.parseLong(request.getParameter("contactCode"));
  }

  if (action.equals("viewModify")) {
    contact = ContactService.getContact(connection, contactCode, sUser.userCode);
  }

  connection.commit();
  connection.close();

%>

<html>
  <head>
    <title><%= contact != null ? "Modifica" : "Inserisci"%> Contatto</title>
    <meta name="author" content="Mario Zambrini"/>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>

    <link href="addressbook.css" rel="stylesheet" type="text/css"/>     				

    <script language="javascript">

      function isEmpty(value) {

        if (value == null || value.length == 0)
          return true;
        for (var count = 0; count < value.length; count++) {
          if (value.charAt(count) != " ")
            return false;
        }
        return true;

      }

      function isValidEmail(s) {
        if (s.indexOf("@") > 0 && (s.indexOf("@") < (s.length - 1)))
          return true;
        return false;
      }

      function modifyContact() {

        f = document.modifyForm;

        //Nome
        if (isEmpty(f.firstname.value)) {
          alert("Inserire il nome.");
          return;
        }

        //Cognome
        if (isEmpty(f.surname.value)) {
          alert("Inserire il cognome.");
          return;
        }

        //Indirizzo
        if (isEmpty(f.address.value)) {
          alert("Inserire l'indirizzo.");
          return;
        }

        //Citta'
        if (isEmpty(f.city.value)) {
          alert("Inserire la citta'.");
          return;
        }

        //Telefono
        if (isEmpty(f.phone.value)) {
          alert("Inserire il telefono.");
          return;
        }

        //E-Mail
        if (isEmpty(f.email.value) || !isValidEmail(f.email.value)) {
          alert("Inserire una e-mail corretta.");
          return;
        }

        f.submit();
        return;

      }

    </script>
  </head>
  <body bgcolor="#FFFFFF" text="#000000">

    <div id="container">

      <%@include file="include/header.inc" %> 

      <div id="content-container">
        <div id="content">

          <span class="normal"><%= contact != null ? "Modifica" : "Inserisci"%> Contatto</span>
          <br/><br/>      			

          <form name="modifyForm" action="AddressBookView.jsp" method="post">

            <table border="0">
              <tr>
                <td class="normal" width="100">Nome</td>
                <td width="250">
                  <input type="text" name="firstname" 
                         value="<%=contact != null ? contact.firstname : ""%>" 
                         size="20" maxlength="50"/>
                </td>
              </tr>
              <tr>
                <td class="normal" width="100">Cognome</td>
                <td width="250">
                  <input type="text" name="surname" 
                         value="<%=contact != null ? contact.surname : ""%>" 
                         size="20" maxlength="50"/>
                </td>
              </tr>
              <tr>
                <td class="normal" width="100">Sesso</td>
                <td width="250">
                  <input type="radio" name="sex" value="M" 
                         <%=contact == null || (contact != null && contact.sex.equals("M")) ? "checked=\"checked\"" : ""%>
                         />M
                  <input type="radio" name="sex" value="F" 
                         <%=contact != null && contact.sex.equals("F") ? "checked=\"checked\"" : ""%>
                         />F				              
                </td>
              </tr>
              <tr>
                <td class="normal" width="100">Indirizzo</td>
                <td width="250">
                  <input type="text" name="address" 
                         value="<%=contact != null ? contact.address : ""%>" 
                         size="20" maxlength="50"/>
                </td>
              </tr>
              <tr>
                <td class="normal" width="100">Citt&#224;</td>
                <td width="250">
                  <input type="text" name="city" 
                         value="<%=contact != null ? contact.city : ""%>" 
                         size="20" maxlength="50"/>
                </td>
              </tr>
              <tr>
                <td class="normal" width="100">Telefono</td>
                <td width="250">
                  <input type="text" name="phone" 
                         value="<%=contact != null ? contact.phone : ""%>" 
                         size="20" maxlength="50"/>
                </td>
              </tr>
              <tr>
                <td class="normal" width="100">E-Mail</td>
                <td width="250">
                  <input type="text" name="email" 
                         value="<%=contact != null ? contact.email : ""%>" 
                         size="20" maxlength="50"/>
                </td>
              </tr>             
              <tr>
                <td class="normal" width="100">&#160;</td>
                <td width="250">
                  <input type="button" value="Ok" onClick="modifyContact()"/>
                  <input type="button" value="Annulla" onClick="backForm.submit()"/>
                </td>
              </tr>
            </table>

            <% if (contact != null) {%>
            <input type="hidden" name="contactCode" value="<%=contact.contactCode%>" />
            <%}%>
            <input type="hidden" name="selectedInitial" value="<%=selectedInitial%>"/>  
            <input type="hidden" name="action" value="<%=contact != null ? "modify" : "insert"%>" />

          </form>

        </div>

        <%@include file="include/aside.inc" %> 
        <%@include file="include/footer.inc" %> 

      </div>
    </div>     

    <form name="backForm" method="post" action="AddressBookView.jsp">
      <input type="hidden" name="selectedInitial" value="<%=selectedInitial%>"/>  
      <input type="hidden" name="action" value="view"/>
    </form>

  </body>
</html>