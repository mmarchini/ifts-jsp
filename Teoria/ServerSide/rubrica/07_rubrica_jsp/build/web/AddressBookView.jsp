
<%@ page info="Gestione Contatti" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page errorPage="/ErrorPage.jsp" %>
<%@page import="blogics.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>

<%int i;
  String action;
  Connection connection = null;
  String[] initials;
  Contact contact;
  Contact[] contacts;

  User sUser = (User) session.getAttribute("sUser");
  boolean loggedOn = (sUser != null);

  Class.forName("com.mysql.jdbc.Driver");
  connection = DriverManager.getConnection(
          "jdbc:mysql://localhost/rubrica?user=rubrica&password=rubrica");
  connection.setAutoCommit(false);

  action = request.getParameter("action");
  if (action == null) {
    action = "view";
  }

  /* ---- Parameters ------ */
  String selectedInitial = request.getParameter("selectedInitial");
  String firstname = request.getParameter("firstname");
  String surname = request.getParameter("surname");
  String sex = request.getParameter("sex");
  String address = request.getParameter("address");
  String city = request.getParameter("city");
  String phone = request.getParameter("phone");
  String email = request.getParameter("email");
  Long contactCode = null;
  if (request.getParameter("contactCode") != null) {
    contactCode = Long.parseLong(request.getParameter("contactCode"));
  }

  if (action.equals("insert")) {
    contact = new Contact(sUser.userCode, firstname, surname, sex,
            address, city, phone, email);
    contact.insert(connection);
  }

  if (action.equals("modify")) {
    contact = ContactService.getContact(connection, contactCode, sUser.userCode);
    contact.firstname = firstname;
    contact.surname = surname;
    contact.sex = sex;
    contact.address = address;
    contact.city = city;
    contact.phone = phone;
    contact.email = email;
    contact.update(connection);
  }

  if (action.equals("delete")) {
    contact = ContactService.getContact(connection, contactCode, sUser.userCode);
    contact.delete(connection);
  }

  /* ------ VIEW -------- */

  ArrayList<String> vInitials = new ArrayList<String>();
  ArrayList<Contact> vContacts = new ArrayList<Contact>();
  vInitials = ContactService.getInitials(connection, sUser.userCode);
  initials = (String[]) vInitials.toArray(new String[0]);

  if (selectedInitial == null) {
    selectedInitial = "*";
  }

  vContacts = ContactService.getContacts(connection, sUser.userCode,
          (selectedInitial.equals("*") ? null : selectedInitial), null);
  contacts = (Contact[]) vContacts.toArray(new Contact[0]);

  /* ------ END VIEW ----- */

  connection.commit();
  connection.close();

%>

<html>
  <head>        
    <title>Rubrica</title>      
    <meta name="author" content="Mario Zambrini"/>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <link href="addressbook.css" rel="stylesheet" type="text/css"/>     				

    <script language="javascript">

      function changeInitial(inital) {
        document.changeInitialForm.selectedInitial.value = inital;
        document.changeInitialForm.submit();
        return;
      }

      function insertContact() {
        document.insertForm.submit();
        return;
      }

      function modifyContact(contactCode) {
        document.modifyForm.contactCode.value = contactCode;
        f = document.modifyForm.submit();
        f.submit();
        return;
      }

      function deleteContact(code) {
        document.deleteForm.contactCode.value = code;
        document.deleteForm.submit();
        return;
      }

    </script>
  </head>

  <body>

    <div id="container">

      <%@include file="include/header.inc" %> 

      <div id="content-container">
        <div id="content">

          <a href="javascript:insertContact();">Nuovo Contatto</a>
          <br/><br/>
          <%if (selectedInitial.equals("*")) {%>
            <span class="selectedInitial">*&#160;</span>
          <%} else {%>
              <a class="initial" href="javascript:changeInitial('*');">*&#160;</a>
          <%}%>      
          <%for (i = 0; i < initials.length; i++) {
              if (initials[i].equals(selectedInitial)) {%>
                <span class="selectedInitial"><%=initials[i]%>&#160;</span>
            <%} else {%>
                <a class="initial" href="javascript:changeInitial('<%=initials[i]%>');"><%=initials[i]%>&#160;</a>
            <%}%>  
          <%}%>
          <br/><br/>

          <dl class="multiple-table">
            <%for (i = 0; i < contacts.length; i++) {%>  
            <dt>
              <a href="javascript:deleteContact(<%=contacts[i].contactCode%>)"><img src="images/delete.gif" border="0"/></a>
              <a href="javascript:modifyContact(<%=contacts[i].contactCode%>)">
                <%=contacts[i].surname%>, <%=contacts[i].firstname%>                  
              </a>              
            </dt>            
            <dd class="first"><%= contacts[i].phone%></dd>
            <dd><%= contacts[i].address%></dd>
            <dd><%= contacts[i].city%></dd>          
            <%}%>
          </dl>


        </div>

        <%@include file="include/aside.inc" %> 
        <%@include file="include/footer.inc" %> 

      </div>
    </div>     

    <form name="changeInitialForm" method="post" action="AddressBookView.jsp">
      <input type="hidden" name="selectedInitial"/>
      <input type="hidden" name="action" value="view"/>      
    </form>    

    <form name="insertForm" method="post" action="AddressBookInsertModify.jsp">
      <input type="hidden" name="selectedInitial" value="<%=selectedInitial%>"/>
      <input type="hidden" name="action" value="viewInsert"/>     
    </form>     

    <form name="modifyForm" method="post" action="AddressBookInsertModify.jsp">     
      <input type="hidden" name="contactCode" value=""/>               
      <input type="hidden" name="selectedInitial" value="<%=selectedInitial%>"/>    
      <input type="hidden" name="action" value="viewModify"/>
    </form>      

    <form name="deleteForm" method="post" action="AddressBookView.jsp">     
      <input type="hidden" name="contactCode"/>      
      <input type="hidden" name="selectedInitial" value="<%=selectedInitial%>"/>    
      <input type="hidden" name="action" value="delete"/>
    </form>       

  </body>
</html>
