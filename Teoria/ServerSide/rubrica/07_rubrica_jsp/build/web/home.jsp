<%@page info="Home Page" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="/ErrorPage.jsp" %>
<%@page import="java.sql.*"%>
<%@page import="blogics.*"%>

<%User sUser = (User) session.getAttribute("sUser");
  boolean loggedOn = (sUser != null);

  int i;
  String action;
  Connection connection = null;

  Class.forName("com.mysql.jdbc.Driver");
  connection = DriverManager.getConnection(
          "jdbc:mysql://localhost/rubrica?user=rubrica&password=rubrica");
  connection.setAutoCommit(false);

  String userCode = request.getParameter("userCode");
  String password = request.getParameter("password");

  action = request.getParameter("action");
  if (action == null) {
    action = "view";
  }

  if (action.equals("logon")) {
    sUser = UserService.getUser(connection, userCode);
    if (sUser != null && sUser.password.equals(password)) {
      session.setAttribute("sUser", sUser);
      loggedOn = true;
    }
  }

  if (action.equals("logout")) {
    if (loggedOn) {
      session.removeAttribute("sUser");
      sUser=null;      
      loggedOn = false;
    }
  }

  connection.commit();
  connection.close();

%>

<html>

  <head>
    <title>Rubrica - Logon</title>
    <meta name="author" content="Mario Zambrini"/>
    <link href="addressbook.css" rel="stylesheet" type="text/css"/>

    <script language="javascript">

      function isEmpty(value) {

        if (value == null || value.length == 0)
          return true;
        for (var count = 0; count < value.length; count++) {
          if (value.charAt(count) != " ")
            return false;
        }
        return true;

      }

      function submitLogon() {

        if (isEmpty(document.logonForm.userCode.value)) {
          alert("Inserire uno username.");
          return;
        }

        if (isEmpty(document.logonForm.password.value)) {
          alert("Inserire una password.");
          return;
        }

        document.logonForm.submit();
      }

    </script>
  </head>

  <body>

    <div id="container">

      <%@include file="include/header.inc" %> 

      <div id="content-container">
        <div id="content">
          
          <%if (!loggedOn) {%>
          <h2>Logon</h2>
          <br/><br/>

          <form name="logonForm" action="home.jsp" method="post">

            <table border="0">
              <tr>
                <td class="normal" width="100">Username</td>
                <td width="250">
                  <input type="text" name="userCode" size="20" maxlength="50"/>
                </td>
              </tr>
              <tr>
                <td class="normal" width="100">Password</td>
                <td width="250">
                  <input type="password" name="password" size="20" maxlength="50"/>
                </td>
              </tr>
            </table>
            <input type="hidden" name="action" value="logon"/>     
            <input type="button" value="Ok" onClick="submitLogon();"/>
          </form>
          <%}%>

          <%if (loggedOn) {%>
          <h2>Home</h2>
          <br/><br/>
          Benvenuto <%=sUser.firstname%> <%=sUser.surname%>!<br/>
          <form name="logoutForm" action="home.jsp" method="post">
            <input type="hidden" name="action" value="logout"/>
          </form>
          <a href="javascript:logoutForm.submit()">Logout</a>
          <%}%>	
          
        </div>
        
        <%@include file="include/aside.inc" %> 
                
      </div>
        <%@include file="include/footer.inc" %> 
    </div>        

  </body>

</html>
