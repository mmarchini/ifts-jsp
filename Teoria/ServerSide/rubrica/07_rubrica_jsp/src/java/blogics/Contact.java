package blogics;

import java.sql.*;

import util.*;


public class Contact  {
  
  public Long contactCode;
  public String userCode;
  public String firstname;
  public String surname;
  public String sex;
  public String address;
  public String city;
  public String phone;
  public String email;
  public String active;
  public Timestamp timestamp;
  
  
  public Contact(String userCode, String firstname,
  String surname, String sex, String address,
  String city, String phone, String email) {    
    this.userCode=userCode;
    this.firstname=firstname;
    this.surname=surname;
    this.sex=sex;
    this.address=address;
    this.city=city;
    this.phone=phone;
    this.email=email;    
  }
  
  public Contact(ResultSet resultSet) {    
    try {contactCode=new Long(resultSet.getLong("CD_CONTACT")); } catch (SQLException sqle) {}
    try {userCode=resultSet.getString("CD_USER");} catch (SQLException sqle) {}
    try {firstname=resultSet.getString("NOME");} catch (SQLException sqle) {}
    try {surname=resultSet.getString("COGNOME");} catch (SQLException sqle) {}
    try {sex=resultSet.getString("SEX");} catch (SQLException sqle) {}
    try {address=resultSet.getString("VIA");} catch (SQLException sqle) {}
    try {city=resultSet.getString("CITTA");} catch (SQLException sqle) {}    
    try {phone=resultSet.getString("TEL");} catch (SQLException sqle) {}
    try {email=resultSet.getString("EMAIL");} catch (SQLException sqle) {}
    try {active=resultSet.getString("FL_ACTIVE");} catch (SQLException sqle) {}
    try {timestamp=resultSet.getTimestamp("TIMESTAMP");} catch (SQLException sqle) {}    
  }
    
  public void insert(Connection connection) throws SQLException {

    String sql;
    Statement statement;
    ResultSet resultSet;
    
    /* generazione CD_CONTACT */                 
         
    sql="SELECT MAX(CD_CONTACT) AS N FROM ACONTATTI"; 
     
    statement = connection.createStatement();
    resultSet=statement.executeQuery(sql);

    if (resultSet.next())
      contactCode = new Long(resultSet.getLong("N")+1);
    else
      contactCode = new Long(1);

    resultSet.close();
          
    /*  inserimento nuovo contatto */    
    sql=" INSERT INTO ACONTATTI "+
        "   ( CD_CONTACT,CD_USER,"+
        "     NOME,COGNOME,"+
        "     SEX,VIA,"+
        "     CITTA,TEL,"+
        "     EMAIL,FL_ACTIVE "+
        "   ) "+
        " VALUES ("+
        contactCode+","+
        "'"+Conversion.getDatabaseString(userCode)+"',"+
        "'"+Conversion.getDatabaseString(firstname)+"',"+
        "'"+Conversion.getDatabaseString(surname)+"',"+
        "'"+Conversion.getDatabaseString(sex)+"',"+
        "'"+Conversion.getDatabaseString(address)+"',"+
        "'"+Conversion.getDatabaseString(city)+"',"+
        "'"+Conversion.getDatabaseString(phone)+"',"+
        "'"+Conversion.getDatabaseString(email)+"',"+
        "'S')";
    
    statement=connection.createStatement(); 
    statement.executeUpdate(sql);
    
  }
  
  
  public void update(Connection connection) 
    throws SQLException {

    String sql;
    Statement statement;
        
    sql=" UPDATE ACONTATTI "+
        " SET "+
        "   NOME = '"+Conversion.getDatabaseString(firstname)+"', "+
        "   COGNOME = '"+Conversion.getDatabaseString(surname)+"', "+
        "   SEX= '"+Conversion.getDatabaseString(sex)+"',"+
        "   VIA = '"+Conversion.getDatabaseString(address)+"', "+
        "   CITTA = '"+Conversion.getDatabaseString(city)+"', "+
        "   TEL = '"+Conversion.getDatabaseString(phone)+"', "+
        "   EMAIL = '"+Conversion.getDatabaseString(email)+"', "+
        "   FL_ACTIVE = '"+Conversion.getDatabaseString(active)+"' "+
    
        " WHERE "+
        "   CD_CONTACT = "+contactCode+" ";
    
    statement=connection.createStatement(); 
    statement.executeUpdate(sql);
        
  }
  
  public void delete(Connection connection) throws SQLException {
    
    String sql;
    Statement statement;
    
    sql=" UPDATE ACONTATTI "+
        " SET FL_ACTIVE='N' "+
        " WHERE "+
        " CD_CONTACT="+contactCode.toString();
    
    statement=connection.createStatement(); 
    statement.executeUpdate(sql);        
    
  }
  
}