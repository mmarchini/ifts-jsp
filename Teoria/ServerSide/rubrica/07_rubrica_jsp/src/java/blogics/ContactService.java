package blogics;

import java.util.*;
import java.sql.*;

import util.*;

public class ContactService {

  private ContactService() {
  }

  public static ArrayList<String> getInitials(Connection connection, String userCode)
          throws SQLException {

    String initial;
    ArrayList<String> initials;
    String sql;
    Statement statement;
    ResultSet resultSet;

    initials = new ArrayList<String>();

    sql = " SELECT DISTINCT UCase(Left(ACONTATTI.COGNOME,1)) AS INITIAL "
            + " FROM ACONTATTI "
            + " WHERE "
            + "   CD_USER='" + Conversion.getDatabaseString(userCode) + "' "
            + "   AND FL_ACTIVE='S' "
            + " ORDER BY UCase(Left(ACONTATTI.COGNOME,1))";

    statement = connection.createStatement();
    resultSet = statement.executeQuery(sql);


    while (resultSet.next()) {
      initial = resultSet.getString("INITIAL");
      initials.add(initial);
    }

    return initials;

  }

  public static Contact getContact(Connection connection, Long contactCode, String userCode)
          throws SQLException {

    Contact contact;
    String sql;
    Statement statement;
    ResultSet resultSet;

    sql = " SELECT *"
            + " FROM ACONTATTI "
            + " WHERE "
            + "   CD_USER='" + Conversion.getDatabaseString(userCode) + "' and "
            + "   CD_CONTACT=" + contactCode + " and "
            + "   FL_ACTIVE='S'";

    statement = connection.createStatement();
    resultSet = statement.executeQuery(sql);


    if (resultSet.next()) {
      contact = new Contact(resultSet);
    } else {
      return null;
    }


    return contact;

  }

  public static ArrayList<Contact> getContacts(
          Connection connection, String userCode,
          String initial, String searchString)
          throws SQLException {

    Contact contact;
    ArrayList<Contact> contacts;
    String sql;
    Statement statement;
    ResultSet resultSet;

    contacts = new ArrayList<Contact>();

    sql = " SELECT * FROM ACONTATTI "
            + " WHERE "
            + "   CD_USER='" + Conversion.getDatabaseString(userCode) + "' "
            + "   AND FL_ACTIVE='S' ";
    if (initial != null) {
      sql += " AND UCASE(LEFT(COGNOME,1))='" + initial + "' ";
    }
    if (searchString != null) {
      sql += " AND ( INSTR(COGNOME,'" + searchString + "')>0 ";
      sql += " OR INSTR(NOME,'" + searchString + "')>0 ";
      sql += " OR INSTR(VIA,'" + searchString + "')>0 ";
      sql += " OR INSTR(CITTA,'" + searchString + "')>0 ";
      sql += " OR INSTR(TEL,'" + searchString + "')>0 ";
      sql += " OR INSTR(EMAIL,'" + searchString + "')>0 )";
    }
    sql += "ORDER BY COGNOME, NOME, EMAIL";

    statement = connection.createStatement();
    resultSet = statement.executeQuery(sql);



    while (resultSet.next()) {
      contact = new Contact(resultSet);
      contacts.add(contact);
    }



    return contacts;

  }
}