package blogics;

import java.sql.*;

public class User {

  public String userCode;
  public String firstname;
  public String surname;
  public String password;
  public Timestamp timestamp;  
  
  public User(String userCode, String firstname, String surname, String password) {                  
    this.userCode=userCode;
    this.firstname=firstname;
    this.surname=surname;
    this.password=password;
  }

  public User(ResultSet resultSet) {    
    try {userCode=resultSet.getString("CD_USER");} catch (SQLException sqle) {}
    try {firstname=resultSet.getString("NOME");} catch (SQLException sqle) {}
    try {surname=resultSet.getString("COGNOME");} catch (SQLException sqle) {}
    try {password=resultSet.getString("PWD");} catch (SQLException sqle) {}
    try {timestamp=resultSet.getTimestamp("TIMESTAMP");} catch (SQLException sqle) {}    
  }
}