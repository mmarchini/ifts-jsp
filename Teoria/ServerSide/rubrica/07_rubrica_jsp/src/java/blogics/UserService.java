package blogics;

import java.sql.*;

public class UserService {

  private UserService() {}

  public static User getUser(Connection connection, String userCode)
    throws SQLException {

    User user=null;
    Statement statement;
    ResultSet resultSet;

    String sql=" SELECT * " +
                "   FROM AUTENTI " +
                " WHERE " +
                "   CD_USER = '" + util.Conversion.getDatabaseString(userCode)+"'";
    statement = connection.createStatement();
    resultSet = statement.executeQuery(sql);
    
    if (resultSet.next())
      user=new User(resultSet);
    resultSet.close();

    return user;

  }
  
}