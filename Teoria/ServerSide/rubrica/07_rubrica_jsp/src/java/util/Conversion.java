package util;

public class Conversion {

  /**
   * From ' to '' for Oracle queries
   *
   * @param inputString The String to convert
   * @return The converted String
   */
  public static String getDatabaseString(String inputString) {

    return inputString.replace("'","''");

  }
}
