CREATE DATABASE  IF NOT EXISTS `rubrica` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `rubrica`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: rubrica
-- ------------------------------------------------------
-- Server version	5.1.43-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autenti`
--

DROP TABLE IF EXISTS `autenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autenti` (
  `CD_USER` varchar(100) NOT NULL DEFAULT '',
  `PWD` varchar(100) DEFAULT NULL,
  `NOME` varchar(100) DEFAULT NULL,
  `COGNOME` varchar(100) DEFAULT NULL,
  `CD_LNG` varchar(3) DEFAULT NULL,
  `TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`CD_USER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autenti`
--

LOCK TABLES `autenti` WRITE;
/*!40000 ALTER TABLE `autenti` DISABLE KEYS */;
INSERT INTO `autenti` VALUES ('ar','ar','Alessio','Ravani','ENG','2002-03-06 00:01:05'),('mz','mz','Mario','Zambrini','ITA','2002-03-08 15:52:47');
/*!40000 ALTER TABLE `autenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acontatti`
--

DROP TABLE IF EXISTS `acontatti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acontatti` (
  `CD_CONTACT` int(11) NOT NULL DEFAULT '0',
  `CD_USER` varchar(100) DEFAULT NULL,
  `NOME` varchar(100) DEFAULT NULL,
  `COGNOME` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(200) DEFAULT NULL,
  `VIA` varchar(200) DEFAULT NULL,
  `CITTA` varchar(100) DEFAULT NULL,
  `TEL` varchar(50) DEFAULT NULL,
  `SEX` varchar(1) DEFAULT NULL,
  `FL_ACTIVE` varchar(1) DEFAULT NULL,
  `TIMESTAMP` datetime DEFAULT NULL,
  PRIMARY KEY (`CD_CONTACT`),
  KEY `acontatti_fk_cd_user` (`CD_USER`),
  CONSTRAINT `acontatti_fk_cd_user` FOREIGN KEY (`CD_USER`) REFERENCES `autenti` (`CD_USER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acontatti`
--

LOCK TABLES `acontatti` WRITE;
/*!40000 ALTER TABLE `acontatti` DISABLE KEYS */;
INSERT INTO `acontatti` VALUES (1,'ar','Mario','Rossi','mrossi@unife.it','Via Bologna, 1','Ferrara','339 88 22 112','M','S','2012-03-25 00:00:00'),(2,'ar','Antonio','Verdi','averdi@unife.it','Via Roma, 2','Messina','347 22 88 1123','M','S','2012-03-25 00:00:00'),(3,'ar','Paola','Gialli','pgialli@unife.it','Via Venezia, 9','Milano','02 88 9283 22','F','S','2012-03-25 00:00:00'),(4,'mz','Giovanni','Bianchi','gbianchi@unife.it','Via Firenze, 45','Genova','+39 347 33 123 456','M','S','2012-03-25 00:00:00'),(5,'mz','Marco','Neri','mneri@unife.it','Via Toscana, 36','Bologna','051 339988','M','S','2012-03-25 00:00:00'),(6,'mz','Paolo','Rossi','a@a.it','Via ...','Ferrara','0532 999999','M','N','2012-03-25 00:00:00'),(7,'ar','Luca','Verdi','b@b.it','aaa','aaa','333 9977554','M','S','2012-03-25 00:00:00'),(8,'mz','Gino','Neri','a@a.it','a','a','11','M','S','2012-03-25 00:00:00'),(9,'mz','Luigi','Marrone','a@a.it','a','b','1','M','S','2012-03-25 00:00:00'),(10,'ar','Gino','Violetti','m@m@it','a','c','333','M','S',NULL),(11,'mz','Paolo','Rossi','a@a.it','a','b','1','M','S',NULL),(12,'mz','a','b','a@a.it','A','B','1','M','N',NULL),(13,'mz','a','a','a@a.it','a','a','1','F','N',NULL);
/*!40000 ALTER TABLE `acontatti` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-03-25 18:15:24
